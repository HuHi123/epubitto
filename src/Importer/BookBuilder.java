/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Importer;

import Database.DBManager;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;
import proje.Book;
import proje.Utils;
import Shelves.Library;
import java.sql.SQLException;
import proje.BookFactory;
import proje.Main;
//import org.jsoup.Jsoup;

/**
 *
 * @author O5shorioush
 */
public class BookBuilder {
    
    private BookBuilder(){}
    public static boolean validate(String path){
        File mime = new File(path + File.separator + "mimetype");
        File meta = new File(path + File.separator + "META-INF");

        if (!mime.exists() || !mime.isFile() || !meta.exists() || !meta.isDirectory()) {
            return false;
        }

        File container = new File(path + File.separator + "META-INF" + File.separator + "container.xml");
        if (!container.exists() || !container.isFile()) {
            return false;
        }
        String st;
        try {
            try (BufferedReader br = new BufferedReader(new FileReader(mime))) {
                st = br.readLine();
                br.close();
            }
        } catch (IOException e) {
            return false;
        }
        return st.equals("application/epub+zip");
    }

    public static Book build(String path) throws FileNotFoundException, IOException {
        String opf = "";
        String title = "";
        String creator = "";
        String publisher = "";
        String identifier = "";
        String desc = "";
        String lang = "";
        String ImLoc = "";
        String ropf="";
        ArrayList<String> flow = new ArrayList<>();
        try {
            FileInputStream fis = new FileInputStream(new File(path + File.separator + "META-INF" + File.separator + "container.xml"));
            Document doc = Jsoup.parse(fis, "UTF-8", "", Parser.xmlParser());
            Elements f = doc.getElementsByTag("rootfile");
            for (Element g : f) {
                if (g.attr("media-type").equals("application/oebps-package+xml")) {
                    opf = g.attr("full-path");
                    System.out.println(opf);
                    System.out.println(Utils.getParentDir(opf));

                }
            }
        } catch (IOException ex) {
            System.out.println("Couldn't build book :(");
            Logger.getLogger(BookBuilder.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        ////////////////////////////////////getting the book map/////////////////////////////
        FileInputStream opfStream = new FileInputStream(new File(path + File.separator + Utils.getCompatiblePath(opf)));
        Document opf1 = Jsoup.parse(opfStream, "UTF-8", "", Parser.xmlParser());
        //////////////////////////getting metadata////////////////

        String id = "";
        if (!opf1.getElementsByTag​("package").isEmpty()) {
            id = opf1.getElementsByTag​("package").attr("unique-identifier");
        }

        Element meta = opf1.getElementsByTag​("metadata").get(0);

        //assign title
        if (!meta.getElementsByTag​("dc:title").isEmpty()) {
            title = meta.getElementsByTag("dc:title").get(0).text();
            System.out.println(title);
        }
        //assign creator
        if (!meta.getElementsByTag​("dc:creator").isEmpty()) {
            creator = meta.getElementsByTag("dc:creator").get(0).text();
            System.out.println(creator);
        }
        //assign language (lang variable)
        if (!meta.getElementsByTag​("dc:language").isEmpty()) {
            lang = meta.getElementsByTag("dc:language").get(0).text();
            System.out.println(lang);
        }
        //assign publisher
        if (!meta.getElementsByTag​("dc:publisher").isEmpty()) {
            publisher = meta.getElementsByTag("dc:publisher").get(0).text();
            System.out.println(publisher);
        }
        //assign description (desc variable)
        if (!meta.getElementsByTag​("dc:description").isEmpty()) {
            desc = meta.getElementsByTag("dc:description").get(0).text();
            System.out.println(desc);
        }
        //assign identifier
        if (!meta.getElementsByTag​("dc:identifier").isEmpty() && !id.equals("")) {
            for (Element h : meta.getElementsByTag("dc:identifier")) {
                if (h.attr("id").equals(id)) {
                    identifier = h.text();
                    break;
                }
            }
            System.out.println(identifier);
        }

        Element manifest = opf1.getElementsByTag("manifest").get(0);
        ///////////////getting the cover////////
        if (!manifest.getElementsByAttributeValue("properties", "cover-image").isEmpty()) {
            Element im = manifest.getElementsByAttributeValue("properties", "cover-image").get(0);
            ImLoc = path + opfR(opf) + File.separator + Utils.getCompatiblePath(im.attr("href"));
        } else {
            if (!meta.getElementsByTag​("meta").isEmpty()) {
                Elements d =meta.getElementsByTag​("meta");
                for(Element el : d){
                    if(el.attr("name").equals("cover")){
                        String tmp = el.attr("content");
                        if(manifest.getElementById(tmp)!=null){
                            ImLoc = path + opfR(opf) + File.separator +Utils.getCompatiblePath( manifest.getElementById(tmp).attr("href"));
                        }else{
                            ImLoc="";
                        }
                        break;
                    }
                }
            }
        }
        ///////////////getting the flow////////
        Element spine = opf1.getElementsByTag("spine").get(0);
        Elements items = spine.children();
        int i = 0;
        for (Element chapter : items) {
            String idref = chapter.attr("idref");
            if(manifest.getElementById(idref)==null){
                return null;
            }
            String idm = path + opfR(opf) + File.separator + Utils.getCompatiblePath(manifest.getElementById(idref).attr("href"));
            flow.add(idm);
            System.out.println(idm);
            i++;
        }
        System.out.println(i);
        /////////////////////////////////////////////////////////////////////////
        Book book = BookFactory.create(title, path, creator, publisher, identifier, desc, lang, opf, ImLoc, flow);
        Book.coll.put(title, book);
        Library.addBook(book);
        Main.books.add(book);
        try {
            DBManager.storeBook(book);
        } catch (SQLException ex) {
            Logger.getLogger(BookBuilder.class.getName()).log(Level.SEVERE, null, ex);
        }
        return book;
    }
    
    public static String opfR(String opf){
        if(!Utils.getCompatiblePath(opf).contains(File.separator)){
            return "";
        }
        return File.separator + Utils.getParentDir(opf);
    }

}
