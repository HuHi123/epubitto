/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Importer;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.FilenameUtils;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.core.ZipFile;
import proje.Utils;

/**
 *
 * @author O5shorioush
 */
public class Extractor {

    //The temporary directory used to add an Ebook
    public static final String destDir = System.getProperty("user.dir").substring(0, System.getProperty("user.dir").lastIndexOf(File.separator)) + File.separator + "books";

    //Add a whole directory
    public static void unzipRec(File dir) {
        if (dir.isFile() && isEpub(dir)) {
            unzip(dir);
        }
        if (dir.isDirectory()) {
            for (File fileEntry : dir.listFiles()) {
                unzipRec(fileEntry);
            }
        }
    }

    //Add a single Ebook
    public static void unzip(File zipFile) {

        System.out.println(zipFile.toString());
        File newDir=createDir(zipFile); 
        if (newDir == null) {
            System.out.println("no wayy");
            return;
            
        }
        try {
            ZipFile zipFile1 = new ZipFile(zipFile.toString());
            zipFile1.extractAll(newDir.getAbsolutePath());
        } catch (ZipException e) {
            System.out.println("Invalid File :(");
            newDir.delete();
            return;
        }

        System.out.println("Unzipped successfully to " + destDir);

        try {
            BookBuilder.build(newDir.getAbsolutePath());
        } catch (IOException ex) {
            Logger.getLogger(Extractor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    //Remove temporary files used in adding previous ebook
    public static File createDir(File tmp) {
        File dir = new File(destDir);
        if (!dir.exists()) {
            if (!dir.mkdir()) {
                return null;
            }
            System.out.println("la wlo");
        }
//            try {
        //Utils.deleteDirectoryContent(dir);
        
        File g = new File(destDir + File.separator + toFileName(tmp.getName()));
        if (g.exists()) {
            return createDir(new File(tmp.toString() + 1));
        }
        System.out.println("this is");
        System.out.println(g);
        if (g.mkdir()) {
            return g;
        }

//            } catch (IOException e) {
//                System.out.println("Failed to delete :( ");
//                return false;
//            }
        return null;

    }

    //Check if given file is a Epub or not
    public static boolean isEpub(File f) {
        return FilenameUtils.getExtension(f.toString()).equals("epub");
    }
    
    public static String toFileName(String name){
        return name.replaceAll("[^a-zA-Z0-9]","");
    }

    //No Broken Hearts In The Club No Tears In the Club, 'Cause We Gon' Get It Poppin' Tonight !!!!!!!!!
}
