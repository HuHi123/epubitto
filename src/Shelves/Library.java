/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shelves;

import Reader.Viewer;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javax.imageio.ImageIO;
import proje.Book;
import proje.Utils;

/**
 *
 * @author O5shorioush
 */
public class Library {

    private static VBox Library;
    public static String Theme = "glass";
    static int c = 0;
    static Image shelfImage = new Image("Themes/"+ Theme+"Shelf.png");
    static Insets shelfMargin = new Insets(Utils.relativeHeight(50), 0, 0, 0);
    static DropShadow shelfShadow = new DropShadow(BlurType.THREE_PASS_BOX, Color.web("#000000d8"), Utils.relativeHeight(101.3475), 0.2, Utils.relativeWidth(10), Utils.relativeHeight(10));
    static Image genericImage = new Image("proje/generic.jpg");
    static Insets bookMargin = new Insets(0, 0, 0, Utils.relativeWidth(80));
    static DropShadow bookShadow = new DropShadow(BlurType.THREE_PASS_BOX, Color.web("#0000008a"), Utils.relativeHeight(9.705), 0, Utils.relativeWidth(10), Utils.relativeHeight(20));

    public static VBox getLibrary() {
        if (Library == null) {
            initialize();
        }
        return Library;
    }

    private Library() {
    }

    public static void initialize() {
        Library = new VBox();
        Library.setAlignment(Pos.TOP_CENTER);
        Library.prefHeight(Utils.relativeHeight(721));
        Library.prefWidth(Utils.relativeWidth(1267));
        Library.setPadding(new Insets(Utils.relativeHeight(50), 0, Utils.relativeHeight(50), 0));
        Library.getChildren().add(createEmptyShelf());
    }

    public static void addBook(Book book) throws IOException {
        VBox shelf;
        HBox content;
        if (Library == null) {
            initialize();
        }
        if (c % 6 == 0) {
            if (c == 0) {
                shelf = (VBox) Library.getChildren().get(0);
                content = (HBox) shelf.getChildren().get(0);
                content.getChildren().add(getImageView(book, true));
            } else {
                shelf = createEmptyShelf();
                content = (HBox) shelf.getChildren().get(0);
                content.getChildren().add(getImageView(book, true));
                Library.getChildren().add(shelf);
            }
        } else {
            shelf = (VBox) Library.getChildren().get(Library.getChildren().size() - 1);
            content = (HBox) shelf.getChildren().get(0);
            content.getChildren().add(getImageView(book, false));
        }
        c++;
        System.out.println("added");

    }

    private static VBox createEmptyShelf() {
        VBox shelf = new VBox();
        HBox content = new HBox();
        ImageView raf = new ImageView();
        shelf.setAlignment(Pos.TOP_CENTER);
        shelf.prefHeight(Utils.relativeHeight(700));
        shelf.prefWidth(Utils.relativeWidth(100));
        content.setAlignment(Pos.BOTTOM_CENTER);
        content.prefHeight(Utils.relativeHeight(221));
        content.prefWidth(Utils.relativeWidth(1304));
        shelf.getChildren().add(content);
        raf.setFitHeight(Utils.relativeHeight(118));
        raf.setFitWidth(Utils.relativeWidth(1502));
        raf.setPickOnBounds(false);////////////////////////////////
        raf.setPreserveRatio(true);
        raf.setImage(shelfImage);
        raf.setEffect(shelfShadow);
        shelf.getChildren().add(raf);
        VBox.setVgrow(raf, Priority.ALWAYS);
        shelf.setPadding(shelfMargin);
        return shelf;
    }

    private static ImageView getImageView(Book book, boolean first) {
        String imageLocation = book.getImLoc();
        ImageView coverImage = new ImageView();
        coverImage.setFitHeight(Utils.relativeHeight(208));
        coverImage.setFitWidth(Utils.relativeWidth(141));
        coverImage.setPickOnBounds(false);
        coverImage.setPreserveRatio(true);
        System.out.println(imageLocation);
        if ("".equals(imageLocation)) {
            coverImage.setImage(genericImage);
        } else {
            BufferedImage bufferedImage;
            try {
                bufferedImage = ImageIO.read(new File(imageLocation));
                Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                coverImage.setImage(image);
            } catch (IOException ex) {
                System.err.println("Failed to raed cover image, setting a generic cover");
                coverImage.setImage(genericImage);
            }
        }
        coverImage.setPreserveRatio(true);
        coverImage.setEffect(new DropShadow(BlurType.THREE_PASS_BOX, Color.web("#0000008a"), Utils.relativeHeight(9.705), 0, Utils.relativeWidth(10), Utils.relativeHeight(20)));
        if (!first) {
            HBox.setMargin(coverImage, bookMargin);
        }

//        coverImage.addEventHandler(MouseEvent.MOUSE_CLICKED, (MouseEvent event) -> {
//            System.out.println(book.getTitle() + "  pressed");
//            event.consume();
//        });
        coverImage.setOnMouseClicked((MouseEvent event) -> {
            System.out.println("Tile clicked " + book.getTitle());
            Viewer.getViewer().open(book);
            event.consume();
        });

        coverImage.setOnMouseEntered((MouseEvent event) -> {
            System.out.println("Tile entered " + book.getTitle());
            DropShadow d = (DropShadow)coverImage.getEffect();
            d.setColor(Color.LIGHTBLUE);
            event.consume();
        });
        coverImage.setOnMouseExited((MouseEvent event) -> {
            System.out.println("Tile exited " + book.getTitle());
            DropShadow d = (DropShadow)coverImage.getEffect();
            d.setColor(Color.web("#0000008a"));
            event.consume();
        });

        return coverImage;
    }
}
