/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shelves;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author O5shorioush
 */
public class ShelfController implements Initializable {

    @FXML
    MenuItem ImFile;

    @FXML
    MenuItem ImDir;

    @FXML
    MenuItem Close;

    @FXML
    VBox vbox1;

    @FXML
    ScrollPane scrollpane1;

    @FXML
    MenuBar mainBar;

    @FXML
    StackPane wait;

    @FXML
    StackPane stack;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ShelfModel sm = new ShelfModel();
        ImFile.setOnAction((ActionEvent t) -> {
            //wait.setVisible(true);
            stack.getChildren().get(0).toFront();
            Loader.ImportFile();
            //wait.setVisible(false);
            stack.getChildren().get(1).toBack();
        });

        ImDir.setOnAction((ActionEvent t) -> {
            //wait.setVisible(true);
            stack.getChildren().get(0).toFront();
            Loader.ImportDir();
            //wait.setVisible(false);
            stack.getChildren().get(1).toBack();
        });

        Close.setOnAction((ActionEvent t) -> {
            System.exit(0);
        });

        sm.initialize(stack, vbox1, mainBar, scrollpane1);
    }

}
