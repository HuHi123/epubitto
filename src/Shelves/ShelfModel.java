/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shelves;

import static Shelves.Library.Theme;
import java.io.File;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author O5shorioush
 */
public class ShelfModel {

    StackPane stp;
    VBox vb;
    MenuBar mb;
    ScrollPane slp;
    public static ObservableList<String> ob;

    public void initialize(StackPane stp, VBox vb, MenuBar mb, ScrollPane slp) {
        this.stp = stp;
        this.vb = vb;
        this.mb = mb;
        this.slp = slp;
        slp.setFitToWidth(true);
        vb.prefWidthProperty().bind(stp.widthProperty());
        vb.prefHeightProperty().bind(stp.heightProperty());
        slp.prefWidthProperty().bind(stp.widthProperty());
        slp.prefHeightProperty().bind(stp.heightProperty());
//        vbox2.prefHeightProperty().bind(scrollpane1.widthProperty());
        Library.initialize();
        VBox vbox2r = Library.getLibrary();
        //special attention
        slp.setContent(vbox2r);
        vbox2r.prefWidthProperty().bind(slp.widthProperty());

        System.out.println(ShelfController.class.getResource("/Themes/" + Library.Theme + ".css").toExternalForm());
//        slp.getStylesheets().add(this.getClass().getResource("/Themes/" + Library.Theme + ".css").toExternalForm());
//        slp.getStyleClass().add("q");
//        slp.setStyle("-fx-background-color:transparent;");
//
//        mb.getStylesheets().add(ShelfController.class.getResource("/Themes/" + Library.Theme + ".css").toExternalForm());
        //applyStyle(slp, mb);
//        mb.getStyleClass().add("w");
        /////////////
        //Library.shelfImage = new Image("Themes/"+ Library.Theme+"Shelf.png");
        slp.getStylesheets().add(this.getClass().getResource("/Themes/" + Library.Theme + ".css").toExternalForm());
        slp.getStyleClass().add("q");
        slp.setStyle("-fx-background-color:transparent;");
        System.out.println("++:"+Library.Theme);
//        slp.getStylesheets().add(".q > .viewport{\n"
//                + "    -fx-background-image: url('glassBack.jpg');\n"
//                + "}\n"
//                + "\n"
//                + ".menu-bar{\n"
//                + " -fx-background-image: url('glassMenu.jpg');\n"
//                + "}\n"
//                + "\n"
//                + ".menu-bar .label{\n"
//                + " -fx-color: #000000 ;\n"
//                + "}\n"
//                + "\n"
//                + ".context-menu{\n"
//                + "    -fx-color: #FFFFFF;\n"
//                + "    -fx-background-image: url('glassMenu.jpg');\n"
//                + "}\n"
//                + "\n"
//                + ".context-menu .label{\n"
//                + "    -fx-color: #000000;\n"
//                + "    \n"
//                + "}");

        mb.getStylesheets().add(ShelfController.class.getResource("/Themes/" + Library.Theme + ".css").toExternalForm());
        mb.getStyleClass().add("w");

        ///////////////
        File folder = new File(System.getProperty("user.dir") + File.separator + "src" + File.separator + "Themes");

        for (File fileEntry : folder.listFiles()) {
            String style = fileEntry.getName();
            if (fileEntry.isFile()) {
                if (style.contains(".css") && !"btnTheme.css".equals(style)) {
                    MenuItem sItem = new MenuItem(style.substring(0, style.indexOf(".css")));
                    sItem.setOnAction(e -> {
                        Library.Theme = style.substring(0, style.indexOf(".css"));
                        System.out.println("++:"+Library.Theme);

                        //ob.clear();
                        //System.out.println("the shett is: " + ob.get(0));
                        //mb.getStylesheets().clear();
                        slp.getStylesheets().add(this.getClass().getResource("/Themes/" + Library.Theme + ".css").toExternalForm());
                        slp.getStyleClass().add("q");
                        slp.setStyle("-fx-background-color:transparent;");
                        mb.getStylesheets().add(ShelfController.class.getResource("/Themes/" + Library.Theme + ".css").toExternalForm());
                        mb.getStyleClass().add("w");
                        Library.shelfImage = new Image("/Themes/"+ Library.Theme+"Shelf.png");
                        //applyStyle(slp, mb);
                    });
                    mb.getMenus().get(1).getItems().add(sItem);
                }
            }
        }

    }

    public void applyStyle(ScrollPane slp, MenuBar mb) {
//        slp.getStylesheets().set(0,this.getClass().getResource("/Themes/" + Library.Theme + ".css").toExternalForm());
//        slp.getStyleClass().set(0,"q");
        ob = slp.getStylesheets();

        slp.getStylesheets().add(this.getClass().getResource("/Themes/" + Library.Theme + ".css").toExternalForm());
        System.out.println("the shett is: " + ob.get(0));
        slp.getStyleClass().add("q");
        slp.setStyle("-fx-background-color:transparent;");
//        mb.getStylesheets().set(0,ShelfController.class.getResource("/Themes/" + Library.Theme + ".css").toExternalForm());
//        mb.getStyleClass().set(0,"w");
        mb.getStylesheets().add(ShelfController.class.getResource("/Themes/" + Library.Theme + ".css").toExternalForm());
        mb.getStyleClass().add("w");
    }
//    public void accept(Visitor v){
//        v.visit(this);
//    }
}
