/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Shelves;

import Importer.Extractor;
import java.io.File;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;

/**
 *
 * @author O5shorioush
 */
public class Loader {

    public static void ImportFile() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        //Set extension filter
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Epub (*.epub)", "*.EPUB"));

        //Show open file dialog
        File file = fileChooser.showOpenDialog(null);
        if (file != null) {
            System.out.println(file.toString());
            Extractor.unzip(file);
        }
    }

    public static void ImportDir() {
        DirectoryChooser dirChooser = new DirectoryChooser();
        dirChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        File dir = dirChooser.showDialog(null);
        if (dir != null) {
            System.out.println(dir.toString());
            Extractor.unzipRec(dir);
        }
    }

    public static void Scan() {

    }
}
