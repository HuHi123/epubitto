/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Database;

import Shelves.Library;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.apache.commons.io.FilenameUtils.separatorsToUnix;
import proje.Book;
import proje.BookFactory;

/**
 *
 * @author O5shorioush
 */
public class DBManager {

    private static DBManager db;
    private static Connection conn;

    private DBManager() {
    }

    public static DBManager getDB() {
        if (db == null) {
            db = new DBManager();
        }
        if (conn == null) {
            System.out.println("connection is null!");
            conn = getConnection();
        }
        if (conn != null) {
            System.out.println("connection is fixed!");
            return db;
        }
        return null;
    }

    private static Connection getConnection() {
        String loc = System.getProperty("user.dir").substring(0, System.getProperty("user.dir").lastIndexOf(File.separator)) + File.separator + "books" + File.separator + "db.dat";
        String url = "jdbc:sqlite:" + separatorsToUnix(loc);
        File dbPath = new File(loc);
        System.out.println("dir is :" + loc);
        if (!dbPath.exists()) {
            System.out.println("path doesnt exist!!");
            if (!dbPath.mkdir()) {
                System.out.println("path created!!");
                return null;
            }
        }
        try {
            Connection con = DriverManager.getConnection(url);
            if (con != null) {
                System.out.println("Connection established!!");
                DatabaseMetaData dbm = con.getMetaData();
                // check if "employee" table is there
                ResultSet tables1 = dbm.getTables(null, null, "books", null);
                if (tables1.next()) {
                    // Table exists
                    System.out.println("Table of name: " + tables1.getString("TABLE_NAME") + "exists!!!");
                } else {
                    // Table does not exist
                    System.out.println("creating books table!");
                    String SQL = "CREATE TABLE books (\n"
                            + "  id INTEGER PRIMARY KEY,\n"
                            + "  title TEXT,\n"
                            + "  loc TEXT,\n"
                            + "  creator TEXT,\n"
                            + "  publisher TEXT,\n"
                            + "  identifier TEXT,\n"
                            + "  desc TEXT,\n"
                            + "  lang TEXT,\n"
                            + "  ImLoc TEXT,\n"
                            + "  opf TEXT\n"
                            + ");";
                    Statement stmt = con.createStatement();
                    stmt.execute(SQL);
                }

                ResultSet tables2 = dbm.getTables(null, null, "flows", null);
                if (tables2.next()) {
                    // Table exists
                    System.out.println("Table of name: " + tables2.getString("TABLE_NAME") + "exists!!!");
                } else {
                    // Table does not exist
                    System.out.println("creating flows table!");
                    String SQL = "CREATE TABLE flows (\n"
                            + "  id INTEGER PRIMARY KEY,\n"
                            + "  bid INTEGER,\n"
                            + "  loc TEXT,\n"
                            + "  seq INTEGER,\n"
                            + " FOREIGN KEY(bid) REFERENCES books(id)"
                            + ");";
                    Statement stmt = con.createStatement();
                    stmt.execute(SQL);
                }
                System.out.println("job successfull!!");
                return con;
            }

        } catch (SQLException e) {
            System.out.println(e.getMessage());

        }
        return null;
    }

//    public static Book retrieveBook(String title) {
//
//        String SQL = "Update Employees SET age = ? WHERE id = ?";
//        PreparedStatement pstmt = DBManager.getConnection().prepareStatement(SQL);
//    }
//
    public static boolean storeBook(Book book) throws SQLException {
        conn.setAutoCommit(false);
        String sql1 = "insert into books (title,loc,creator,publisher,identifier,desc,lang,ImLoc,opf) values (?,?,?,?,?,?,?,?,?) ;";
        PreparedStatement insBook = conn.prepareStatement(sql1, Statement.RETURN_GENERATED_KEYS);
        String sql2 = "insert into flows (bid,loc,seq) values (?,?,?) ;";
        PreparedStatement insFlow = conn.prepareStatement(sql2);
        insBook.setString(1, book.getTitle());
        insBook.setString(2, book.getLoc());
        insBook.setString(3, book.getCreator());
        insBook.setString(4, book.getPublisher());
        insBook.setString(5, book.getIdentifier());
        insBook.setString(6, book.getDesc());
        insBook.setString(7, book.getLang());
        insBook.setString(8, book.getImLoc());
        insBook.setString(9, book.getOpf());
        /////////////////////////////
        insBook.executeUpdate();
        conn.commit();
        int key = 0;
        ResultSet rs = insBook.getGeneratedKeys();
        if (rs != null && rs.next()) {
            key = rs.getInt(1);
            System.out.println("The id is : " + key);
        }
        ArrayList<String> flow = book.getFlow();
        int i = 1;
        for (String st : flow) {
            insFlow.setInt(1, key);
            insFlow.setString(2, st);
            insFlow.setInt(3, i);
            insFlow.executeUpdate();
            conn.commit();
            i++;
        }
        //Supplying Values for PreparedStatement Parameters
//updateSales.setInt(1, e.getValue().intValue());
//        updateSales.setString(2, e.getKey()) ////////////////////////////
//        conn.
        return true;
    }
//  

    public ArrayList<Book> retrieveAll() throws SQLException {
        ArrayList<Book> lib = new ArrayList<>();

        String sql1 = "select * from books ;";
        Statement stmt1 = conn.createStatement();
        ResultSet res1 = stmt1.executeQuery(sql1);
        while (res1.next()) {
            int id = res1.getInt("id");
            String title = res1.getString("title");
            String loc = res1.getString("loc");
            String creator = res1.getString("creator");
            String publisher = res1.getString("publisher");
            String identifier = res1.getString("identifier");
            String desc = res1.getString("desc");
            String lang = res1.getString("lang");
            String ImLoc = res1.getString("ImLoc");
            String opf = res1.getString("opf");
            ArrayList<String> flow = new ArrayList<>();
            String sql2 = "select loc from flows where bid='" + id + "' GROUP BY seq;";
            Statement stmt2 = conn.createStatement();
            ResultSet res2 = stmt2.executeQuery(sql2);
            while (res2.next()) {
                flow.add(res2.getString("loc"));
            }
            Book aux = BookFactory.create(title, loc, creator, publisher, identifier, desc, lang, opf, ImLoc, flow);
            //create(String title,String path,String creator,String publisher,String identifier,String desc,String lang,String opf,String ImLoc,ArrayList<String> flow)

            lib.add(aux);

        }
        return lib;
    }

}
