/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import proje.Book;
import proje.Main;

/**
 *
 * @author O5shorioush
 */
public class Viewer {
    public static Viewer viewer;
    public Book book;
    int c = 0;
    private Viewer(){};
    public Viewer open(Book book){
        
        this.book = book;
        c=0;
        try {
            Main.b.flip();
        } catch (IOException ex) {
            Logger.getLogger(Viewer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return viewer;
    }
    public static Viewer getViewer(){
        if(viewer == null){
            viewer= new Viewer();
        }
        return viewer;
    }
    
    public String next(boolean flip){
        int i = Math.floorMod((c + 1), (book.getFlow().size()));;
        if(flip==true)
            c = i;
        book.getFlow().get(i);
        return book.getFlow().get(i);
    }
    
    public String prev(boolean flip){
        int i = Math.floorMod((c - 1), (book.getFlow().size()));
        System.out.println("Nummmmmber:"+i);
        System.out.println(book.getFlow().size());
        if(flip==true)
            c = i;
        book.getFlow().get(i);
        return book.getFlow().get(i);
    }
    
    public String current(){
        return book.getFlow().get(c);
    }
    public Book getBook(){
        return book;
    }
    
    public String goTo(int chapter , boolean flip){
        if(c >= book.getFlow().size() || c<0){
            return null;
        }
        int i = chapter;
        if(flip==true)
            c = i;
        return book.getFlow().get(i);
    }
}
