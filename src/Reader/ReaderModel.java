/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reader;

import com.jfoenix.controls.JFXButton;
import java.io.File;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebView;
import proje.Main;
import proje.Utils;

/**
 *
 * @author O5shorioush
 */
public class ReaderModel {
    public  void goRight(WebView qwerty){
        Viewer.getViewer().next(true);
            qwerty.getEngine().load(new File(Viewer.getViewer().current()).toURI().toString());
    }
    public  void goLeft(WebView qwerty){
        Viewer.getViewer().prev(true);
            qwerty.getEngine().load(new File(Viewer.getViewer().current()).toURI().toString());
    }
    public  void initialize(StackPane sp, WebView qwerty, BorderPane hb){
        sp.setAlignment(Pos.CENTER);
        StackPane.setAlignment(hb, Pos.CENTER);
        hb.prefWidthProperty().bind(Main.b.f.widthProperty());
        hb.prefHeightProperty().bind(Main.b.f.heightProperty());
        
        sp.setOnKeyPressed((KeyEvent event) -> {
            switch (event.getCode()) {
                case LEFT:    goLeft(qwerty); break;
                case RIGHT:   goRight(qwerty); break;
                case A:    goLeft(qwerty); break;
                case D:   goRight(qwerty); break;
            }
        });

        double butHeight = Utils.relativeHeight(600);
        double butWidth = Utils.relativeWidth(100);
        JFXButton bu12= new JFXButton();
        Insets in = new Insets(Utils.relativeHeight(30),Utils.relativeHeight(30),Utils.relativeHeight(30),Utils.relativeHeight(30));
        bu12.alignmentProperty().set(Pos.CENTER);
        bu12.setMaxHeight(butHeight);
        bu12.setMaxWidth(butWidth);
        bu12.setMinHeight(butHeight);
        bu12.setMinWidth(butWidth);
        bu12.setPrefHeight(butHeight);
        bu12.setPrefWidth(butWidth);
        bu12.setText("<");
        bu12.setStyle("-fx-font-size:" + Utils.relativeHeight(49));
        bu12.setMnemonicParsing(false);
        bu12.setOnAction((ActionEvent event) -> {
            goLeft(qwerty);
        });
        HBox.setMargin(bu12, in);
        
        JFXButton bu22= new JFXButton();
        bu22.alignmentProperty().set(Pos.CENTER);
        bu22.setMaxHeight(butHeight);
        bu22.setMaxWidth(butWidth);
        bu22.setMinHeight(butHeight);
        bu22.setMinWidth(butWidth);
        bu22.setPrefHeight(butHeight);
        bu22.setPrefWidth(butWidth);
        bu22.setText(">");
        bu22.setStyle("-fx-font-size:" + Utils.relativeHeight(49));
        bu22.setMnemonicParsing(false);
        
        bu22.setOnAction((ActionEvent event) -> {
            goRight(qwerty);
        });

               bu12.getStylesheets().add(this.getClass().getResource("/Themes/btnTheme.css").toExternalForm());
        bu22.getStylesheets().add(this.getClass().getResource("/Themes/btnTheme.css").toExternalForm());
        HBox.setMargin(bu22, in);
        BorderPane.setAlignment(bu12, Pos.CENTER);
        BorderPane.setAlignment(bu22, Pos.CENTER);
        hb.setLeft(bu12);
        hb.setRight(bu22);
        hb.setPickOnBounds(false);
        System.out.println(new File(Viewer.getViewer().current()).toURI().toString());
        qwerty.setZoom(2);
        qwerty.getEngine().load(new File(Viewer.getViewer().current()).toURI().toString());
        qwerty.getEngine().executeScript("window.scrollTo(0, document.body.scrollHeight);");
    }
}
