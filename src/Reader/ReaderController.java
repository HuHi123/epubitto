/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reader;

import com.jfoenix.controls.JFXButton;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import proje.Utils;

import javafx.scene.web.WebView;
import proje.Main;

/**
 * FXML Controller class
 *
 * @author O5shorioush
 */
public class ReaderController implements Initializable {

    /**
     * Initializes the controller class.
     */
    int c = 0;
    
    @FXML
    private StackPane sp;
    
    @FXML
    private WebView qwerty;
    
    @FXML
    private BorderPane hb;


    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ReaderModel rm = new ReaderModel();
        rm.initialize(sp, qwerty, hb);
    }

}
